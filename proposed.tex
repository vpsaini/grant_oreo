The clone detection approach proposed here focuses on method level
detection of type-4 clones, which we refer to as semantic clones, in a
scalable manner. We aim to build this approach in a way that is
parallelizable and incremental by design. This approach has a machine
learning model as its main component; this component, which we name
Cognitive Metrics-based Component, is trained based on a set of
features at its training stage, and detects clone pairs at its
prediction stage. In order to make this process work effectively, we
introduce three concepts to be proposed and implemented by this
approach: Feature Engineering, Semantic Filter, and Multi-level Input
Partitioning. These concepts are discussed in
Sections \ref{feature_eng}, \ref{input_par},
and \ref{sem_filter}. Then the cognitive metrics-based component is
described in Section \ref{cognit}, followed by the description of the
proposed pipeline for this approach in Section \ref{pipeline}.

\subsubsection{Feature Engineering} \label{feature_eng}

The input to the cognitive component of this approach is a set of
features that are capable of distinguishing clone pairs from non-clone
pairs. These features are derived from a set of software metrics. Some
general examples of software metrics derived from methods are the
number of lines of code, cyclomatic complexity, number of loops,
etc. As a result of generating features, a dataset with feature
vectors is generated; each feature vector is in the form of \{method1,
method2, feature 1, feature 2,.., feature N\}.

At the very first step, the set of features that are useful for this
process should be selected. This will be done by exploring several
software metrics and the extent to which they are capable of capturing
information that is useful in clone detection. We collect the set of
useful metrics and also develop a metrics calculator as a part of this
research that can calculate the needed metrics. We aim to develop the
metrics calculator in a way that new metrics can be easily added to
it. After calculating the metrics for each method, features for method
pairs need to be calculated. These features will capture the extent to
which a method is different from the other method in terms of software
metrics. Also, these features should be able to demonstrate the
similarities and differences among clone pairs and non-clone pairs,
and act as means of distinguishing clone pairs from non-clone
pairs. Hence, a robust and meaningful formula for calculating features
based on software metrics will be devised.

Also, for many algorithms in machine learning it is advised to remove
the features which are highly
correlated~\cite{hall1999correlation}. Hence, after preparing the
first version of features, a correlation study will be conducted to
reduce the number of necessary dimensions.

\subsubsection{Multi-level Input Partitioning}\label{input_par}

From a very broad perspective, each method can be a candidate clone
for all methods in the repository. Thus, to prepare feature vectors,
each method should be paired with all methods available in the
repository to form all possible clone candidates. This will result in
the problem of Candidate Explosion as the number of candidates grows
quadratically with the number of methods. Many of these candidate
pairs have a drastic difference and are not possible to be clone
pairs. In order to further reduce the number of candidates, we are
going to propose \textit{Multi-level Input Partitioning} to partition
input methods based on different measures. An instance of a very
simple, yet effective measurement is method size: the idea is that two
methods which are very different in their sizes are very likely
implementing different functionalities and so they should not be
clones. We define method size as the number of tokens each method has,
where tokens are language keywords, literals (strings literals are
split on whitespace), and identifiers. We borrowed the definition of
tokens from~\cite{sajnani2016sourcerercc}.

Thus, our plan is to make partitions of possible query and candidates
based on some defined measurements (such as method size) at different
levels, and only do the method pairing locally inside the lowest level
partitions. The reason for performing partitioning at multiple levels
is that it is possible to have different-sized partitions, which means
some partitions being bigger than others. To make sure that we can
load these partitions in memory, we further partition the bigger
partitions horizontally into smaller sub-partitions, and continue this
until we can easily load each partition into the memory. These
smaller partitions are then indexed in memory one at a time. The
queries in a partition are then executed on all of its
sub-partitions. This way we do not have to query the entire dataset,
but only a smaller subset of it, which reduces the number of
candidates while maintaining the recall. The idea of input
partitioning is not new, and has been used in information-retrieval
many times
~\cite{livieri:2007icse,cambazoglu2006effect,kulkarni2010document}. Researchers
in other fields have explored partitioning based on size and also
horizontal partitioning to solve the scalability and speed
issues~\cite{liebeherr1993effect}. Moreover, Svajlenko and Roy used
horizontal partitioning to do in-memory clone-detection and achieved
very high speed~\cite{svajlenko2017fast}. As per our knowledge we
would be the first to propose and use multi-level partitions of
indexes for clone detection.

\subsubsection{Semantic Filter}\label{sem_filter}

Software metrics mostly capture the structural properties of code
which are indeed very useful in detecting semantic clones, yet not
enough. Hence, including semantical features would assure us that the
detected clone pairs share both semantical and structural
properties. However, we believe that we can make use of the semantical
information in tackling the problem of candidate explosion as well. As
a result, instead of adding semantical features to the set of
features, we utilize a \textit{Semantic Filter} which filters
candidates with low potential of being clones before feeding them to
the cognitive component. This filter will be developed using
Information Retrieval techniques and will leverage various
characteristics of code to derive semantical information. So far, we
have explored the semantical information that can be inferred from
library calls and referenced attributes inside a method. Other
examples of semantic filter that need thorough exploration include but
are not limited to parsing method names, paying attention to
application specific keywords, types of arguments, and types of
exceptions handled in methods. This filter itself will consist of
various sub-filters, each focusing on a semantical feature to filter
out non-relevant pairs.

This filter helps us produce a small index by generating fewer but
semantically similar candidates. In fact, we plan to design this
filter in a way that while eliminating many candidates, it can
maintain a high recall. Also by reducing the number of candidates,
this filter contributes to making the proposed approach both fast and
scalable. However, an important step here is to conduct a sensitivity
study to analyze the optimum thresholds for input partitioning and
also for the Semantic Filter.

\subsubsection{Cognitive Metrics-Based Component}\label{cognit}

\begin{figure}
	\centering
	\fbox{\includegraphics[scale=0.6] {Feature_eng_pipeline_nsf.pdf}}
	\caption{Feature Generation Pipeline Process}
	\label{fig:feat-pipeline}
\end{figure}

As typical for machine learning, the cognitive component used by this
approach will have two main stages: \textit{Model Training} stage
followed by \textit{Prediction} of clones in the \textit{Clone
Detection} stage. In \textit{Model Training} stage, the machine
learning model is trained based on feature vectors. The \textit{Clone
Detection} stage uses the trained model to predict clones on a given
dataset. A \textit{Feature Generation} component is common to both
train and prediction stages.

We are going to investigate both supervised and unsupervised learning
for the purpose of training the cognitive component. For supervised
learning, we need \textit{is\_clone} labels for each candidate pair to
be added to its feature vector. This can be done manually or by
utilizing a clone detector tool. Many clone detection techniques which
are accurate but hard to scale, can be used here. Hence, this approach
can be a way of scaling accurate but not scalable approaches too. For
unsupervised learning, we plan to train the model in a way that it can
distinguish clusters with similar feature ranges from other
clusters. These clusters can form groups of clone or non-clone pairs.

\subsubsection{Process Pipeline}\label{pipeline}

We plan to design this approach in the form of a pipeline. We explain
the main components of this pipeline through the rest of this section.

\textbf{Feature Generation Pipeline.} The pipeline for generating the features is depicted in Figure~
\ref{fig:feat-pipeline}. As this figure shows, source code files are first given to the Metrics Calculator
to obtain the Metrics file. In order to generate the feature vectors
from this file, Multi-level Input Partitioning is conducted. We then
create in-memory inverted index based on the defined Semantic Filter
for each partition.

As a Query Q is picked from a partition, the inverted index for this
partition is queried based on the Semantic Filter. If Q meets the
similarity threshold for any candidate, it is paired with that
candidate, and added to the list of pairs. At the end of this process,
the list of possible candidates is generated for Q. Then, Feature
Generation is conducted in order to derive the Feature Vectors for Q,
where for each metric in Q and the corresponding metric in candidates,
the features are generated.

\textbf{Model Training Stage.} 
The training process is where the clone detector model is trained
based on the Feature Vectors. The whole pipeline is conducted only
once on the train dataset, and the model is built. In this process,
Feature Vectors generated by Feature Generation pipeline are stored in
a file and the file is fed to the training process.

An important aspect of training stage in our approach is that we are
going to train multiple models based on different characteristics of
data. We believe that a single model trained for all types of clones
cannot work ideally; this is because different types of clones have
different characteristics, and hence, the potential range of features
varies. As a result, we will categorize code clones based on some
defined criteria, and then train an individual model for each of
them. We also have some ideas of incorporating models from an ensemble
of accurate clone detectors that specialize in certain kinds of
clones, in order for the accuracy to be even better.

In addition, we believe that detecting Type-1 and Type-2 clones does
not need any machine learning model; these types of clones are very
easy to catch using simple Information Retrieval techniques, and
including them in training the models would had unnecessary
complexity. Hence, our plan is to detect these types using some
techniques from Information Retrieval before feeding candidate pairs
to the clone detection stage. As a result, no model is trained for
these types, and the focus of model training will be on Type-3 and
Type-4 ones.

\textbf{Clone Detection Stage.}
Clone detection, which is the decision making part of the process and
where the machine learning model makes its predictions, will also be
carried out by making use of the Feature Generation pipeline. However,
in the Prediction stage, prediction will be a part of the pipeline and
it will be conducted for each feature vector once it is generated. In
other words, after receiving each Feature Vector from the Feature
Generation pipeline, the decision on whether it is a clone or not, is
made.

\subsubsection{Precision/Recall Benchmark}\label{benchmark}

Clone detection work has benefited greatly from benchmarks such as
BigCloneBench~\cite{svajlenko2017cloneworks}. Unfortunately,
BigCloneBench focuses only on recall. When it comes to measuring
precision, there is no commonly accepted benchmark, and every
researcher makes their own measurements.  Precision relies on a
subjective task of classification: human judges need to decide which
candidate clone pair is a true clone pair. The decision as to which
pair is forming a clone can be made after a certain number of judges
have agreed to mark a pair as a clone pair. The lack of a standard
benchmark for conducting precision evaluation on clone detectors
motivated us want to develop a tool for this purpose.

We propose to develop a semi-automated tool that demands human
judgement in some steps, but that accumulates those judgements, from
many researchers, over time. To this end, we will develop a networked
precision evaluation tool based on BigCloneBench. BigCloneBench is a
dataset of 25,000 Java systems, containing over 8 million clone pairs
that were validated by different
judges~\cite{svajlenko2015evaluating}. The benchmark tags {\em some}
method pairs as clones, but it does not tag {\em all} clone pairs in
the dataset. We plan to do that.

The envisioned
interaction with the tool is as follows. A user (researcher) will
submit the clone pairs reported by their clone detectors to the
precision tool; the tool then will take a statistically significant
sample from these uploaded clone pairs; then the tool looks for the
clone pairs contained in this sample in the BigCloneBench's database
and marks those that are present in the database as true
positives. The pairs that are not reported by the database as true
clones, will be shown to at least three judges who are introduced by
the user. The judges then will decide which pairs are clones, and the
tool will take the majority vote to tag a pair as true positive or
false positive. The tool will track the ideas of judges participating
in its precision studies in a separate database. As the tool is used
by different people over time, if a candidate clone pair reaches a
threshold of N judges tagging it as a true positive, it will be added
to the main database so that it is automatically counted as a true
positive in future studies.

%% \begin{figure}
%% 	\centering
%% 	\fbox{\includegraphics[scale=0.4] {Precision_framework_Architecture.pdf}}
%% 	\caption{Framework for precision benchmark.}
%% 	\label{fig:benchmark}
%% 	\vspace{-0.225in}
%% \end{figure}

We will need to decide on the value of N by analyzing and studying the
range of trustable values. With time, the database of manually
verified clone pairs will grow and help researchers conduct precision
studies in fewer and fewer time. Moreover, this manually labeled
dataset will be of great importance to the researchers who use machine
learning techniques to build and analyse clone detectors.
