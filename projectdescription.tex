\section{Project Description}

\subsection{Overview, Objectives, and Significance of the Project}

\subsubsection{Overview}  

As the scale and complexity of software projects increases, so does
the need of tools that help manage that complexity. One type of tool
that has been getting significant attention recently is clone
detection. Several studies have shown that open source software has
significant code duplication~\cite{Kamiya:2002:CMT:636188.636191,
  Mockus:2007, Roy:2010:NFC:1779593.1779596,
  Ossher:2011:FCO:2117694.2119721}. Our recent study of
GitHub~\cite{Lopes:oopsla2017} has unveiled massive amounts of
file-level duplication, with the JavaScript ecosystem comprising only
of 6\% distinct files and 94\% duplicates of those files. Although
proprietary software code bases are harder to study, it is likely that
this phenomenon of code duplication also affects them to some extent.

Over the years, many clone detection techniques have been proposed,
including our own SourcererCC~\cite{sajnani2016sourcerercc} -- a
token-based, highly scalable clone detector that is publicly
available. However, the focus has been on detecting {\em syntactic}
similarity. This focus is justifiable: syntactic similarity is
concerned with features that exist on the surface of the source code,
and whose constituents are easily extracted by simple text parsing
and/or static analysis techniques. Detecting semantic clones is a much
harder proposition, because it implies an understanding of behavior of
the code, possibly written in different languages. This requires not
only deeper analysis, but also wider observation of relations between
syntax and behavior. For example, the two methods shown in
Listing~\ref{lst:semclones} both substitute a given substring of an input
string. The fact that the analysis of semantic similarities requires more
complexity and range also affects negatively its potential for scale,
and makes semantic clone detection potentially less usable in
practice.


\begin{lstlisting} [caption=Clone Pair Example: 1,label=lst:semclones] 
public String replaceExpression(String input, String replace, String replacement) {
	int idx;
	if ((idx = input.indexOf(replace)) == -1) {
		return input;
	}
	boolean finished = false;
	while (!finished) {
		StringBuffer returning = new StringBuffer();
		while (idx != -1) {
			returning.append(input.substring(0, idx));
			returning.append(replacement);
			input = input.substring(idx + replace.length());
			idx = input.indexOf(replace);
		}
		returning.append(input);
		input = returning.toString();
		if ((idx = returning.indexOf(replace)) == -1) {
			finished = true;
		}
	}
	return input;
}

String substitute(String input, String var, String value) throws IOException {
	StringBuffer out = new StringBuffer();
	int varlen = var.length();
	int oidx = 0;
	for (;;) {
		int idx = input.indexOf(var, oidx);
		if (idx == -1)
		break;
		out.append(input.substring(oidx, idx));
		idx += varlen;
		out.append(value);
		oidx = idx;
	}
	out.append(input.substring(oidx));
	return out.toString();
}

\end{lstlisting}


To this end, we propose a scalable clone detection approach that is
capable of detecting not just syntactically but also semantically
similar fragments of source code. The key insights behind the proposed
approach are threefold: (1) functionally similar pieces of code tend
to share semantically similar tokens, for instance as embodied in the
functions they call and the state they access; (2) not all pieces of
code that share sementically similar tokens are functionally similar;
and (3) functionally similar pieces of code tend to have comparable
metrics. We hope to develop a technique that learns, by examples, a
combination of features that can predict whether two pieces of code
that do the same actions are semantic clones of each other.

Figure~\ref{fig:overview} illustrates the overview of the proposed
approach. For semantic similarity, we use a semantic filter to capture
similar method calls, for example. To this end, we will devise a novel \textit{Action
  Filter} which filters out a large number of method pairs that do not
seem to be doing the same actions, focusing only on the candidates
that do. The potential clones that pass through the Action Filter go through
a machine-learning, metrics-based model that predicts whether they are
clones or not. Preliminary experiments demonstrate that this
approach is both highly accurate and scalable.

\begin{figure}
	\centering
	\fbox{\includegraphics[scale=0.8] {Process_outline.pdf}}
	\caption{Overview of the approach.}
	\label{fig:overview}
	\vspace{-0.225in}
\end{figure}

\subsubsection{Objectives}

The main objective of the proposed research is to make a scalable
clone detector capable of detecting all types of clones, including
syntactic and semantic ones, and including cross-language
clones. Although some existing clone detection approaches do tackle
the difficult niche of semantic clones, these approaches are not
scalable to very large datasets. We focus on scalability, because that
is important to projects that involve very large source code: a clone
detector that doesn't scale well with the size of input is not is of
limited use in practice. With that in mind, we list below some of the
intermediate objectives of this proposal:

\begin{itemize}
	
\item \textbf{Semantic filter}. One objective of the
  proposed research is to develop techniques for semantic filtering
  which will capture semantic similarities between methods. The filter
  is useful as it also tackles the inherent problem of candidate
  explosion by filtering out candidate pairs that are semantically
  different. 

\item \textbf{Multilevel input partitioning}. Another
  objective is to partition the input into multiple levels, for
  example a Tree. This partition will reduce the number of candidates
  for a query as only a smaller subtree needs to be queried. This will
  further help to address the candidate explosion issue. The smaller
  subtrees also ensure that they can be loaded into memory, thereby
  providing faster access. 

\item \textbf{Minimum set of metrics suitable for clone
  detection}. The input to the cognitive component of this approach is
  a set of features that are capable of distinguishing clone pairs
  from non-clone pairs. These features are derived from a set of
  software metrics. Many software metrics are highly correlated. We
  aim to create a small and easy to compute set of metrics.

\item \textbf{Process-pipeline for scalable,
  incremental, and accurate metric-based and learning-based clone
  detection}. Metrics and learning based techniques usually suffer
  from high false positive rate. Moreover, they suffer from candidate
  explosion, which limits them to smaller datasets. We aim to develop
  a process pipeline architecture to achieve scalable, fast clone
  detection using metrics and machine learning techniques. The
  architecture will ensure that the proposed approach scales to ultra
  large repositories, such as GitHub. 

\item \textbf{Process-pipeline to learn from slow but
  accurate clone detector tools and scale to large datasets}. Many
  clone detection techniques, like graph-based and AST-based, are accurate
  but harder to scale. In case of supervised learning, we will use
  these accurate clone detectors to train our cognitive
  component. This way we aim to achieve the higher accuracy while
  scaling up these hard to scale techniques.

\item \textbf{Exploration of unsupervised machine learning
  approaches}. We aim to explore the feasibility and effectiveness of
  both supervised and unsupervised learning approaches. Supervised
  approaches have the benefit of learning from accurate clone
  detectors and producing robust and reliable results. The advantage
  with unsupervised techniques is that such approaches can remove the
  effort required in selecting the metrics for feature generation, and
  also for obtaining the class labels. We aim to explore the
  effectiveness of various supervised and unsupervised learning
  algorithms, and also the state of the art deep learning algorithms
  in this context.

\item \textbf{Precision/recall benchmark}. Although a
  benchmark exists for recall
  (BigCloneBench~\cite{svajlenko2015evaluating}), there is no
  benchmark for measuring the precision of code clone detectors. We
  propose to develop such benchmark, so that we can evaluate our
  proposed clone detector. We will make this benchmark widely
  available for other researchers to use.
	
\end{itemize}
\noindent

\subsubsection{Significance}

According to Rattan et al.~\cite{Rattan20131165}, at least 70 diverse
tools/techniques have been presented in the literature for clone
detection. Over the past 20 years, researchers have studied the
effects of clones, compared cloned code with non-cloned code, and have
proposed clone management techniques. The studies, however, have been
focused primarily on syntactic clones, mostly because of the
unavailability of any scalable semantic clone detector. A scalable
semantic clone detector opens the door to new research opportunities,
such as enabling researchers to study the pervasiveness of Type-4
clones, build techniques to automatically locate and replace low
performing algorithms with high performance ones, identify evolution
patterns in Type-4 clones, find bug patterns and their association
with different semantic implementations, or auto-correct bugs or
suggest alternatives.

The proposed research will also have an impact on developers,
who detect and manage clones to maintain software quality, 
and to prevent new bugs and reduce development risks and
costs~\cite{6747168,roy:queens:07}. Large-scale clone detection can be
used for mining library candidates~\cite{ishihara2012inter}, detecting
similar mobile applications~\cite{chen2014achieving}, license
violation detection~\cite{german2009code,Koschke:CSMR:12}, reverse
engineering product lines~\cite{german2009code,hemel:2012wcre},
finding the provenance of a component~\cite{julius:2011:msr}, and code
search~\cite{kawaguchi2009shinobi,keivanloo:2011wcre}. Industry can
save multi-billions of CPU-computations and hence money by replacing
inefficient algorithms by their efficient but semantically similar
counterparts.

\noindent

\subsection{Background and Related Work}
\label{sec:related}
\input{bg_relatedworks}

\input{priorwork}

\subsection{Proposed Research Work}
%\label{sec:proposed-work}
\input{proposed}

\subsection{Preliminary Work}
\label{sec:preliminary}
\input{prelim}


\subsection{Education and Outreach}
The infrastructure and methods used in this project will be used in
the UCI graduate course INF 212, Analysis of Programming Languages. In
this course, students learn about different paradigms of programming
and how a specific functionality can be implemented using various
programming paradigms. Since this project focuses on semantic clones
and identifying code fragments that do the same thing in very
different ways, the methods and techniques used in this research will
be very useful in that course.
These students will also be involved in testing alpha versions of the
precision benchmark.

We expect to publish several papers on the results and methods of this
project. All graduate students involved in this project will
participate in conferences and workshops relevant to this topic. The
data gathered will be made publicly available, and results from this
project will be presented in several scientific venues in the United
States and around the world. The final tool and its source code will
also be made publicly available.

\subsection{Project Plan}

In the first year of this research, we plan to focus on exploring
various software metrics and their usability in the context of
semantic clone detection. We will collect a comprehensive list of
software metrics, examine their effectiveness in detecting clones, and
remove the correlated ones. Next, we will build the metrics calculator
component of this approach which calculates necessary metrics by
parsing the source code. We will design a robust and
effective formula for creating features based on the metrics. 
We will run experiments to understand how the cognitive metrics-based
component of this approach should be designed; and we will investigate
the usability of supervised and unsupervised learning methods for this
problem. Also in the first year, we will begin the development of the
precision framework, without which it is difficult to measure progress.

In the second year, we plan to focus on the semantic filter
and devising the effective implementation of multi-level input
partitioning. In order to design the semantic filter, we will explore
various characteristics of code and apply information retrieval
techniques to them to find a set of factors that constitute our
semantic filter. Multi-level input partitioning also needs to be
explored thoroughly to find an optimum number of partitions and the
criteria to perform the partitioning at each level. Finally, at the
end of this year, we will explore the categorization of clones in
order to train multiple machine learning models.

We plan to finalize and implement the proposed approach as a tool in
the third year. To this aim, all mentioned components and techniques
will be implemented and integrated into the proposed process
pipeline. Finally, we will evaluate the tool with our benchmark, and
report the numbers. We plan to conduct a large scale precision study
at this step too; we will present results to more judges to get
assured about the precision of our approach. Also in the final year,
we will run our tool on a massive dataset: GitHub. This will be
similar to our recent study that used
SourcererCC~\cite{sajnani2016sourcerercc}, but it will focus on
semantic clones, including cross-language clones.


\subsection{Response to the NSF Evaluation Criteria}
\noindent
\textbf{Intellectual Merit:} Detecting semantic clones has always been
a difficult research question, and researchers have made some attempts
to solving this problem. The primary result of this work -- scalable
detection of semantic clones, including across languages -- will be a
significant step toward addressing this problem. The secondary
contribution is the precision/recall benchmark, something that is
sorely missing in this area of research. Also, some steps of this
research have an important value for the community, specifically the
thorough analysis of software metrics for the purpose of clone
detection, and the exploration of the use of supervised versus
unsupervised learning approaches for clone detection. In addition, the
semantic filter proposed by this work will open new possibilities for
research in this area related to mapping between APIs in different
language ecosystems.


\noindent
\textbf{Broader Impacts:} This research will enable researchers to
process very large datasets for detecting all types of clones. Once
detected, the knowledge of where clones exist offers opportunities to
develop additional tools for software maintenance, including
techniques to replace low performing algorithms with high performance
ones, identify evolution patterns in semantic clones, detect bug
patterns and their association with the different semantic
implementations, auto-correct bugs appearing in multiple instances of
the same code, and many more. The creation of a precision/recall
benchmark for all types of clone detection will be done by
undergraduate and graduate students, as part of the pedagogical goals
of courses that already exist. Besides serving their education, the
benchmark will be publicly available to the research community, so
that others can use it to compare the performance of future clone
detectors. Additionally, we will perform clone detection on millions
of existing GitHub repositories, and will make the results of our scan
publicly available, so that the developers of those projects can act
on the unveiled clones, if they so wish.


\subsection{Results from Prior NSF Awards}

\textbf{Prof. Lopes} (UCI PI) has received a CAREER award from
NSF (7/2004--6/2009) entitled ``A Linguistic Approach to Software
Development.'' With this award she has continued this work with an
emphasis towards assessment of new approaches to software design and
development. Her CAREER award has supported a variety of work in
software and beyond~\cite{lopes:aosd05,lopes:taosd06,jurdak:cst04,jurdak:senet05,jurdak:wnss05,jurdak:tmc07,jurdak:icps04,mandal:ccnc05,jurdak:oceans06,lopes:mc2r06}. The CAREER award also helped Dr. Lopes jump start Sourcerer~\cite{Linstead:2009dl,Bajracharya:2009xp,Ossher:2009rb}. 

Besides the CAREER award, in the past five years, Dr. Lopes is, or has
been, PI on three NSF grants, and co-PI on a fourth one. {\bf
  CCF-1018374} (8/2010 -- 7/2015) funded a large-scale empirical study
of the correlation between open source component utilization and
software quality metrics. The most important findings were the nature
of copy-and-paste in open source~\cite{Ossher:icsm11} and
structure-based search~\cite{Bajracharya:2010}; among several other
smaller publications. {\bf CCF-1218228} (9/2012 -- 8/2016) explored
the use of machine learning techniques in architecture recovery. This
project led us to detecting similar pieces of code, and was the basis
for the development of SourcererCC~\cite{sajnani2016sourcerercc}. {\bf
  CCF-1526593} (6/2015 -- 6/2018) is enabling the development of a
framework for large-scale simulations inspired by Aspect-Oriented
Programming. Publications include~\cite{Valadares:WinterSim16} and
\cite{Lopes:jot17}. {\bf CNS-1730229} (6/2017 -- 6/2018) is a planning
project towards an infrastructure called the National Java Resource
(NJR), which will be a collection of (on the order of) 10,000 Java
projects that build and run out of the box, and for which popular
static and dynamic analysis tools succeed and have cached outputs.
