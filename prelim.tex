We have implemented a first proof of concept of the proposed approach in order to shed
light on its strengths and weaknesses. In order to calculate the
software metrics, we made use of version 6 of the JHawk tool
~\cite{urlJHawk}. JHawk has been widely used in academic studies on
Java metrics
~\cite{gupta2005fuzzy,alghamdi2005oometer,benestad2006assessing,arisholm2006predicting,arisholm2007data}. Table~\ref{tab:metric-breakdown}
shows the $25$ metrics for which we computed values using JHawk. Many
of these metrics are standard metrics. A set of metrics are derived
from the Software Quality Observatory for Open Source Software
(SQO-OSS)~\cite{Samoladas:2008pb}. SQO-OSS is composed of
well-established and validated software quality metrics, which can be
computed either from source code or from surrounding community data.

\label{sec:quality-metrics}
\begin{table}[!tbp]
	\begin{center}
		\caption{Software Metrics Used in the Preliminary Work}
		\resizebox{8cm}{!}{
			\begin{tabular} {l l l}
				\thickhline
				\hlx{v}
				Name & Description \\
				\hlx{vhv}
				\hlx{vhv}
				XMET & External methods called by the method\\
				VREF & Number of variables referenced\\
				VDEC & Number of variables declared\\
				TDN & Total depth of nesting\\
				NOS & Number of statements\\
				NOPR & Total number of operators\\
				NOA & Number of arguments\\
				NLOC & Number of lines of code\\
				NEXP & Number of expressions\\
				NAND & Total number of operands\\
				MOD & Number of modifiers\\
				MDN & Method, Maximum depth of nesting\\
				LOOP & Number of loops (for,while)\\
				LMET & Local methods called by the method\\
				HVOL & Halstead volume\\
				HVOC & Halstead vocabulary of method\\
				HLTH & Halstead length of method\\
				HEFF & Halstead effor to implement a method\\
				HDIF & Halstead difficulty to implement a method\\
				HBUG & Halstead prediction of number of bugs\\
				EXCT & Number of exceptions thrown by the method\\
				EXCR & Number of exceptions referenced by the method\\
				CREF & Number of classes referenced\\
				COMP & McCabes cyclomatic complexity\\
				CAST & Number of class casts\\
				
				\thickhline
			\end{tabular}
		}
		
		\label{tab:metric-breakdown}
	\end{center}
%	\vspace{-0.245in}
\end{table}

Once the metrics were selected, we proceeded to feature calculation;
for this purpose, we calculated the percentage difference for each
metric in a method pair. Percentage difference shows the percentage to
which two methods are different with respect to each other. This
measurement has some shortcomings though. For instance, a problem with
percentage difference is that when two features have a small value,
even with a very small difference, their Percentage Difference gets
large. Consider 1 and 2 which have 50\% percentage difference; this
value equals to the percentage difference between 10 and 20; however,
these two cases are different. In preliminary experiments, we added a
constant number to the formula to balance the final value; however, as
we continue this work, we need to design a more robust and meaningful
formula for calculating features that can work well with different
values of software metrics.  We implemented the multi-level input
partitioning in two levels, and at both levels, the measurement for
making partitions were methods sizes. Thus, we made partitions of
possible query and candidates based on their sizes, and did the method
pairing only inside each partition.

The next concept that needed to be implemented was the semantic
filter. After exploring possible semantic information that can be
gained from source code, we proposed a novel filter,
named \textit{Action Filter}, that captures the fine grained actions a
method does to attain its final result. The logic behind this filter
is that if two cloned methods are doing the same thing, they may call
the same class library methods and reference same class
attributes. This is because when one copies a method, and changes it
to fit her needs, she will not change the library calls and attributes
referenced from class libraries because they are providing a
functionality that does not need to be changed or re-written. Hence,
we elicited library calls and referenced attributes inside methods,
named them \textit{Action Tokens}, and utilized these tokens to
capture the semantic similarity between methods. In fact, the
confidence of rejecting a pair increases as the similarity between the
Action Tokens of this pair decreases. This allowed us to set a
threshold parameter on their similarity. We used overlap-similarity to
calculate the similarity between the Action Tokens of two methods.

To measure the impact of the Action Filter and two-level input
partitioning on the number of candidates, we selected 1,000 random
methods as queries from the dataset. We then executed these 1,000
queries on our system to see the impact of the Action filter and input
partitioning on the number of candidates to be sent to the
metrics-based cognitive component. The threshold for Action filter was
set to 75\%. Also we selected 7 as the number of partitions for input
partitioning. Table~\ref{tab:candidates} summarizes the impact. The top row shows
the base line case where each query is paired with every method
in the dataset, except for itself. This is the {\em modus operandi} of
many clone detection tools. In the second row, the
Action-filter's similarity was set to 1\% to minimize it's impact,
however, partitions were turned on. For the results in third row, we
had kept the Action filter on at 75\% similarity threshold but we
switched off the partitioning. The bottom row shows the results for
number of candidates when both Action filter and input partitioning
were switched on. We can see that Action filter has a strong impact
on reducing the number of candidate pairs. The partitioning lets us do
in-memory computations; together they both help us achieve high
speed and scalability.

\begin{table}
	\scriptsize 
	\centering
	\tabcolsep=0.10cm
	\caption{Impact of Action Filter and Input Partitioning}
	\label{tab:candidates}
	\begin{tabular}{ccc}
		\toprule
		Action Filter & Input Partitioning & Num-Candidates \\
		\midrule
		No Filter & No partitions & 1,559,493,000 \\
		1\% & on & 934,014 \\
		75\% & No partitions & 37,697 \\
		75\% & on & 36,023 \\
		%				\cmidrule{2-7}
		%				& \multicolumn{6}{c}{7868560} \\
		\bottomrule
	\end{tabular}
%	\vspace{-0.225in}
\end{table}

After implementing each of these fine-grained parts of the Feature
Generation pipeline, we proceeded to implementing the cognitive
metrics-based component that consumes the feature vectors generate by
the feature generation pipeline.  We implemented the cognitive
metrics-based component as a classifier trained based
on \textit{is\_clone} labels of each feature vector. In order to
obtain the \textit{is\_clone} labels, we utilized
SourcererCC~\cite{sajnani2016sourcerercc}, our clone
detector that has been shown to have fairly good precision and recall
up to Type-3 clones (but not Type-4).

We defined two model types at this stage: Model-3.1 specific to those Type-3
clones which are more than 90\% similar in size (measured by tokens),
and Model-3.2 specific to other Type-3 clones where the similarity in
size is less than 90\%. In order to train the models, we randomly selected and downloaded 
50k Java projects
from GitHub.

After training the classification models, we proceeded to implementing
the clone detection stage. We noticed that since Type-1 and Type-2
clones are structurally identical, the metric values for these types
are identical too. Therefore, we decided to detect them by comparing
the hash of their metric values. If a candidate is rejected to be a
Type-1 or a Type-2, its Feature Vector is created and then based on
the size difference (number of tokens) in the pair, it is given to
either Model 3.1 or Model 3.2. The model then predicts if the
candidate is a clone pair or not.

Clone detection was conducted on a subset (168,835 methods) of the
dataset used by the Big-CloneEval~\cite{svajlenko2016bigcloneeval}
tool to measure the Recall of this approach in the preliminary
experiments. Table~\ref{tab:recall} summarizes the recall numbers for
SourcererCC and our approach. The recall values are similar for both
approaches in detecting Type-1 and Type-2 clones. However, as it is
observed, our approach has performed much better on Type-3 and even
better on Type-4 clones. This is very encouraging, as it shows that
our approach can accurately identify hard-to-detect Type-4 clones
without hurting the accuracy of detection of other types of clones.


\begin{table}[t]
	\centering
	\small
	\tabcolsep=0.1cm
	\centering
	\caption{BigCloneBench Recall Measurements}\label{tab:recall}
	\begin{tabular}{cccccccc}
		\toprule
		\multirow{2}{*}{Tool}  & & \multicolumn{5}{c}{Clones}  \\
		\cmidrule{2-7}
		& T1  & T2 & VST3 & ST3 & MT3 & WT3/T4  \\
		\midrule
		SourcererCC  & 19,794 & \textbf{6,920}  & 1,803  & \textbf{5,087} & 1177  & 230  \\
		\midrule
		Proposed Approach & 19,794 & 6,914  & \textbf{1,807}  & 4905 & \textbf{3,413}  & \textbf{11,662} \\
		\bottomrule
	\end{tabular}
	%	\vspace{0.2in}
\end{table}

In the absence of any standard benchmark to measure the precision of
clone detectors, we calculated the precision of our approach manually,
which is a common practice used in measuring code clone detection
tools precision. We randomly selected 384 clone pairs, a statistically
significant sample with 95\% confidence level and 5\% confidence
interval, from the 813,546 detected clone pairs. The validation of
clones were done by three judges. Unlike many classification tasks
that require fuzzy human judgement, this task required following very
strict definitions of what constitutes Type-1, Type-2, Type-3, and
Type-4 clones.  We found the precision for our approach to be
90\%. The analysis of where the approach failed shed a light on many
opportunities for improving it. Specifically, we found that the
majority of reported false positives were method pairs that had in
common only one action: {\em length}. Hence, this semantic signal is
not enough for truly identifying semantic similarity, and we have some
ideas for how to avoid these false positives.

The preliminary recall and precision experiments show that the first
version of our proposed approach is at par with SourcererCC, in terms
of recall for Type-1, Type-2, and first two categories of Type-3
clones. This approach is significantly better to detect
harder-to-detect clones of categories MT3 and WT3/T4 while maintaining
an acceptable precision, which can be improved.  It is interesting to
note here that we trained our model using SourcererCC, a Type-3 clone
detector, and we were able to detect harder-to-find (Type-4)
clones. This indicates that the features generated from metrics could
capture fine details in the difference and similarities of two
methods. This is a positive sign as it opens new possibilities for
harder-to-detect Type-3 and Type-4 clones.

To test the performance of our approach on a large dataset, we
downloaded entire IJaDataset ~\cite{ijadataset}, a large inter-project
Java repository containing 25,000 open-source projects (3 million
source files, 250MLOC) mined from SourceForge and Google Code. After
removing methods with less than 50 tokens and those that JHawk was not
able to produce consistent results for (due to inner and anonymous
classes), we were left with 1,559,494 methods (47MLOC).  We set the
maximum memory to be used by feature generator at 16G. The predictor
on the other hand was set to use maximum memory of 14G.

Our approach took 27 hours to predict 49,686,581 clone pairs. We are
confident that the execution time can be reduced further by applying
some good engineering optimizations to the next versions of this
tool. Moreover, when we relaxed the memory constraint of the predictor
to 80G and launched multiple threads for prediction, the process
finished in 8 hours, showing the highly parallelizable nature of our
approach.

These preliminary experiments are encouraging, but there is still much
to be done to validate the idea. First, the set of metrics is too
large and redudant, as many of the metrics are highly correlated. We
should calculate the minimum set of metrics that is capable of
performing well. Second, the Action Filter needs to be explored much
more carefully. In particular, for cross-language clone detection, we
need to account for different APIs with different words that are doing
the same thing. Third, in our preliminary experiments we used Random
Forest decision trees. We need to explore many more machine learning
algorithms for the cognitive component. Fourth, we haven't done any
experiments across languages. Finally, the lack of a precision
benchmark is a serious impediment to rapidly testing different ideas.
