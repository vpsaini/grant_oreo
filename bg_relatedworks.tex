Code clone detection quantifies the similarity between pieces of code,
known as clones, within or between software systems. Clones are
created for a number of reasons including copy-paste- modify,
convergence of solutions by different actors, plagiarism, and code
generation~\cite{roy:queens:07}. Code clone detection techniques and
tools have long been an area of research as software practitioners
depend on them to detect and manage code clones. Clone management is
important in order to maintain software quality, detect and prevent
new bugs, and also to reduce development risks and
costs~\cite{6747168,roy:queens:07}.

There are many kinds of clone detection approaches and tools,
depending on the goals and granularity of the detection. In the early
days of clone detection, file name matching was frequently used as an
heuristic for finding file-level clones (e.g. \cite{Mockus:2007}),
and, to this day, it is still a valid and simple heuristic for certain
clone detection goals. Most clone detectors, however, look at the
contents of the files, either because they aim at finding
finer-granularity code duplication or because they aim at improving
the detection process (or both). Within these, there are four broad
categories of clone detection approaches, ranging from easy-to-detect
to hard-to-detect clones: textual similarity, lexical similarity,
syntactic similarity, and semantic similarity. The literature refers
to them by the four commonly accepted types of
clones~\cite{bellon,roy:queens:07}:

\begin{itemize}
	\item Type-1 (textual similarity): Identical code fragments, except
	for differences in white-space, layout and comments.
	\item Type-2 (lexical, or token-based, similarity): Identical code
	fragments, except for differences in identifier names and literal
	values.
	\item Type-3 (syntactic similarity): Syntactically similar code
	fragments that differ at the statement level. The fragments have
	statements added, modified and/or removed with respect to each
	other.
	\item Type-4 (semantic similarity): Code fragments that are
	semantically similar in terms of what they do, but possibly
	different in how they do it. This type of clones may have little or
	no lexical or syntactic similarity between them. An extreme example
	of exact semantic similarity that has almost no syntactic
	similarity, is that of two sort functions, one implementing bubble
	sort and the other implementing selection sort.
\end{itemize} 

The various types of clones are defined in terms of similarity
metrics, which means that clone detectors may be looking for exact
matches (100\% similarity on the metric of choice) or relaxed matches
that pass a certain threshold of similarity. In any case, an ideal
clone detector would be able to detect all of these types of clones,
even if specific usages would focus on only some of them at a time. In
practice, clone detectors use a variety of signals from the code
(text, tokens, syntactic features, program dependency graphs, etc.) and
tend to aim for detecting specific types of clones, usually up to
Type-3. Very few of them attempt detecting Type-4 clones.
Here we draw comparisons with the existing techniques used in clone
detection.\\

\noindent
\textbf{Techniques for Type-4 Clone Detection} 

\noindent
Sheneamer and Kalita's work~\cite{Sheneamer:icmla16} comes closest to
this proposal, but the differences are important. They use a
supervised learning approach where they use Abstract Syntax Trees
(AST) and Program Dependency Graphs (PDG) as the basis to generate
features. They use a semantic-aware
technique~\cite{keivanloo2015threshold} to generate the PDGs, which
in-turn is used for making semantic features. A model is then trained
using both semantic and syntactic features, which is then used to
predict Type-3 and Type-4 clones. Our proposed approach differs from
theirs as apart from using different metrics, we do not use static
analysis features to train our models, because they tend to be
computationally intensive (slow). Instead, we use a semantic filter,
named Action Filter, to filter out candidates that are not
semantically similar to the query. This helps us address the candidate
explosion issue and makes it easier to do semantic clone detection on
large datasets. We also demonstrate that our approach is scalable to
large datasets, and is incremental by design.

White et al.~\cite{white2016deep} present a learning approach to
detect clones at method and file levels. They used unsupervised
learning to automatically learn discriminating features of source
code. They reported precision (93\%) of their approach on 398 files
and 480 method-level pairs across eight real-world Java
systems. However, no scalability properties of that tool are reported.
It is mentioned that their implementation could be made parallel and
they are working on it to run on a cluster.

Wei and Li~\cite{ijcai2017-423}, recently proposed a supervised
machine learning approach. In their approach they aim to train a hash
function such that the Hamming distance between the hash codes of a
clone pair is as small as possible, and the distance between non-clone
pairs is as big as possible. They claim that they tested their
approach on two datasets: BigCloneBench and OJClone. However, it is
not clear what datasets they used to train and test their model. Also,
they do not mention anything about the scalability of their approach.\\

\noindent
\textbf{Other Learning-Based Techniques}

\noindent
Davey et al.~\cite{davey1995development} detect clones by training neural networks on
certain features of code blocks. They create feature vectors based on
indentations of each line in the code-block, length of each line in
the code-block, and frequency of keywords used in the code
block. These feature vectors are passed to a self-organizing map to
detect clones.\\

\noindent
\textbf{Metric-Based Techniques} 

\noindent
In the metrics-based techniques, instead of comparing code directly,
different metrics for code-blocks are compared. The granularity of
code-blocks is usually classes, methods, or statements. Metrics are
calculated and then compared over these code-blocks.

Mayrand et al.~\cite{mayrand1996experiment} used Datrix tool to
calculate 21 metrics of functions. They compared these metric values
and functions with similar metric values are identified as clones. The
metrics are compared across four points of comparisons, namely: Name,
Layout, Expressions, and Control flow. Two functions are reported as
similar for a point of comparison only if the absolute difference is
less than or equal to the delta threshold defined for each metric in
that point of comparison. The deltas were defined on the basis of
metric definition and their knowledge of the distribution of that
metric on large scale systems. A similar technique is used by
Patenaude et al. to find method level clones in Java
projects~\cite{patenaude1999extending}.

Kontogiannis et al.~\cite{kontogiannis1995detecting} use five modified versions of
well known metrics that capture data and control flow properties of
programs to detect clones in a certain granularity. They experimented
with two techniques. The first one is based on the pairwise Euclidean
distance comparison of begin-end blocks. The second technique uses
clustering. Each metric is seen as an axis and clustering is performed
per metric axis. Intersection of clusters results into the clone
fragments.

Our proposed technique is similar to these. However, we do not
define rules based on delta thresholds; instead, we uplan to use machine
learning algorithms to train classifiers, which learn these rules from
the features created using software metrics. Moreover, we want our approach
to scale to very large datasets.

Other than metric based and machine learning based techniques, there
exists \textbf{Text-based}~\cite{Johnson:1993kx,johnson:94,ducasse1999language}, \textbf{Token
based}~\cite{baker:1992,baker:1995wcre,kamiya:2002zr,li2006cp,sajnani2016sourcerercc,svajlenko2017cloneworks}, \textbf{Tree
based}~\cite{yang1991identifying,Baxter:1998rt,koschke:2006ast,jiang2007deckard},
and \textbf{Graph-based}
techniques~\cite{krinke2001identifying,komondoor2001using,liu2006gplag,gabel2008scalable,chen2014achieving}. The
details of these techniques can be be found
elsewhere~\cite{croy:2009}.
Of the available techniques and tools, scalability is not a
concern. So far, only Chen's
approach~\cite{chen2014achieving},
SourcererCC~\cite{sajnani2016sourcerercc} and
CloneWorks~\cite{svajlenko2017cloneworks} have been shown to scale to
very large datasets.
